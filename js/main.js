var app= new Vue({
    el:"#Сabb",
    data:{
        products:[{id:1,title:"Broccoli ", short_text:'Broccoli (Brassica oleracea var. italica) is an edible green plant ', image:"imgonline-com-ua-Resize-vRrYsunm8Z.jpg",desc:"Full desc"},
        {id:2,title:"Kohlrabi", short_text:"is a biennial vegetable, a low, stout cultivar of wild cabbage.", image:"imgonline-com-ua-Resize-WagTmCE7b9.jpg",desc:"full desc"},
        {id:3,title:"Cauliflower", short_text:"Cauliflower is one of several vegetables in the species Brassica ", image:"imgonline-com-ua-Resize-5zNfjlyqgxY4.jpg",desc:"full desc"},
        {id:4,title:"Sauerkraut", short_text:"Sauerkraut is finely cut raw cabbage that has been fermented by various ", image:"imgonline-com-ua-Resize-DZ9HQC7PWm6u.jpg",desc:"full desc"},
        {id:5,title:"Kale ", short_text:"Kale is a vegetable with green or purple leaves. It is also called borecole", image:"imgonline-com-ua-Resize-zcbdgAyOJo67gD.jpg",desc:"full desc"}],
        product: [{}],
            cart: [],
            contactFields: [
                { caption: 'Name', text:''},
                { caption: 'Company Name', text:''},
                { caption: 'Position', text:'' },
                { caption: 'City', text:''},
                { caption: 'Country', text:''},
                { caption: 'Telephone', text:''},
                { caption: 'Email', text:''},
                { caption: 'You are a', text:''},
                { caption: 'If other, please specify', text: '' },
                { caption: 'You are interested in', text: '' },
            ],
            btnVisible: 0,
            formVisible: 1,
        },
    
        methods: {
            getProduct: function () {
                if (window.location.hash) {
                    var id = window.location.hash.replace('#', '');
                    if (this.products && this.products.length > 0) {
                        for (i in this.products) {
                            if (this.products[i] && this.products[i].id && id == this.products[i].id)
                                this.product = this.products[i];
                        }
                    }
                }
            },
    
            addToCart: function (id) {
                var cart = [];
    
                if (window.localStorage.getItem('cart')) {
                    cart = window.localStorage.getItem('cart').split(',');
                }
    
                if (cart.indexOf(String(id)) == -1) {
                    cart.push(id);
                    window.localStorage.setItem('cart', cart.join());
                    this.btnVisible = 1;
                }
            },
    
            checkInCart: function () {
                if (this.product && this.product.id && window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id)) != -1) 
                    this.btnVisible = 1;
            },
    
            getCart: function () {
                var storage = [];
                storage = localStorage.getItem('cart').split(',')
                for (i in this.products) {
                    if (storage.indexOf(String(this.products[i].id)) != -1) {
                        this.cart.push(this.products[i])
                    }
                }
            },
            
            removeFromCart: function (id) {
                var storage = [];
                storage = window.localStorage.getItem('cart').split(',')
    
                storage = storage.filter(storageId => storageId != id)
                window.localStorage.setItem('cart', storage.join())
    
                this.cart = this.cart.filter(item => item.id != id)
            },
    
            makeOrder: function () {
                localStorage.clear();
                this.cart.splice(0, this.cart.length)
                this.formVisible = 0
            },
            btnClick: function(event) {
                window.open("contactus.html");
            }
        },
    
        mounted: function () {
            this.getProduct();
            this.checkInCart();
            this.getCart();
        },
    
    });